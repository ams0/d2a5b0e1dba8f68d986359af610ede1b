#!/bin/bash

#Usage: gitrebase.sh <branch to rebase into> <branch to rebase from>
# $> gitrebase devel main

echo "Rebasing branch $1 from branch $2"

git checkout $1
git pull

git fetch origin $2
git rebase origin/$1
git push -u origin $1